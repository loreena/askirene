# Chiedi a Irene

C'ha una risposta a tutto. Ma non è detto che sia una risposta utile.

La trovi su: http://loreena.gitlab.io/askirene

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
