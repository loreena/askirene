import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";

Vue.use(Vuex);

const state = {
  question: ""
};

const mutations = {
  questionAsked(state, value) {
    state.question = value;
  }
};

export default new Vuex.Store({
  state,
  mutations,
  getters
});
