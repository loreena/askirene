module.exports = {
  pwa: {
    name: "AskIrene",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black-translucent",
    themeColor: "#ffffff",
    msTileColor: "#ffffff"
  },

  baseUrl: "/askirene"
};
